from tkinter import *
import sqlite3
import requests
import json
from tkinter import messagebox


root= Tk()
root.title('U06: Interactive NFT program')
root.geometry("500x500")


def escrow():

    global balance

    balance = Tk()
    balance.title('Check your balance')
    balance.geometry('400x400')

    user_balance = Entry(balance,width=30)
    user_balance.grid(row=0,column=1, padx=20,pady=(10,0))
    
    try:
        api_request = requests.get("http://api-mainnet.magiceden.dev/v2/wallets/"+ user_balance.get() +"/escrow_balance")
        api = api_request.json()["balance"]
    
    except Exception as e:

        api = f"Error... {e}"


    user_balance_label = Label(balance, text=api)
    user_balance_label.grid(row=5,column=0,pady=(10,0))

    submit_wallet = Button(balance, text="Submit and check whats in the wallet")
    submit_wallet.grid(row=6, column=0, columnspan=2,pady=10,padx=10,ipadx=100)

# The launchpad is where all the new collections are listed
def launchpad():
    #
    global lpad


    lpad = Tk()
    lpad.title('Launchpad collections')
    lpad.geometry('400x400')

    try:
        api_request = requests.get("http://api-mainnet.magiceden.dev/v2/launchpad/collections?offset=0&limit=200") 
        api = json.loads(api_request.content)

    except EXCEPTION as e:

        api = "Error..."
    
    launchpad_label = Label(lpad, text=api[1]['name'])
    launchpad_label.grid(row=2,column=0)
# create and connect to the database
conn = sqlite3.connect('database.db')

# create cursor
c = conn.cursor()

# create table
'''
c.execute("""CREATE TABLE users (
        first_name text,
        last_name text,
        wallet_address text
        )""")
'''

def update():
    
    conn = sqlite3.connect('database.db')

    c = conn.cursor()

    record_id = delete_box.get()

    c.execute("""UPDATE users SET  
        first_name = :first,
        last_name = :last,
        wallet_address = :wallet
        
        WHERE oid = :oid""",
        {
            'first': f_name_editor.get(),
            'last':l_name_editor.get(),
            'wallet':w_address_editor.get(),
        
            'oid': record_id
        })
    
    conn.commit()

    conn.close()

    # Closes the window when users press save edit
    editor.destroy()


# Create a function to update a record in a new window
def edit():
    global editor
    editor= Tk()
    editor.title('Update a Record')
    editor.geometry("400x300")
    
    conn = sqlite3.connect('database.db')

    c = conn.cursor()
    
    record_id = delete_box.get()

    # Query the database
    c.execute("SELECT * FROM users WHERE oid = " + record_id)
    records = c.fetchall()
    
    # Create Global variables for text box names
    global f_name_editor
    global l_name_editor
    global w_address_editor
    
    # Create text boxes
    f_name_editor = Entry(editor,width=30)
    f_name_editor.grid(row=0,column=1, padx=20,pady=(10,0))

    l_name_editor = Entry(editor, width=30)
    l_name_editor.grid(row=2, column=1)

    w_address_editor = Entry(editor, width=30)
    w_address_editor.grid(row=4, column=1)

    # create text box labels
    f_name_label = Label(editor, text="First Name")
    f_name_label.grid(row=0,column=0,pady=(10,0))

    l_name_label = Label(editor, text="Last Name")
    l_name_label.grid(row=2,column=0)

    w_address_label = Label(editor, text="Wallet address")
    w_address_label.grid(row=4,column=0)

    # Loop thru results
    for record in records:
        f_name_editor.insert(0,record[0])
        l_name_editor.insert(0,record[1])
        w_address_editor.insert(0,record[2])
    
    # Create update button
    edit_btn = Button(editor, text="Save Edit", command=update)
    edit_btn.grid(row=6,column=0, columnspan=2,pady=10,padx=10,ipadx=143)

    conn.commit()

    conn.close()


# Create a function to delete a record
def delete():
    
    # create and connect to the database
    conn = sqlite3.connect('database.db')

    c = conn.cursor()

    # Delete a record
    c.execute("DELETE FROM users WHERE oid = " + delete_box.get())
    
    delete_box.delete(0,END)
    
    conn.commit()

    conn.close()

    
# Create submit function for database
def submit():
    
    # create and connect to the database
    conn = sqlite3.connect('database.db')

    c = conn.cursor()
    
    # Insert into table
    c.execute("INSERT INTO users VALUES (:f_name, :l_name, :w_address)",
            {
                'f_name': f_name.get(),
                'l_name': l_name.get(),
                'w_address':w_address.get()


            }
        
        )
    

    conn.commit()

    conn.close()
    
    f_name.delete(0,END)
    l_name.delete(0,END)
    w_address.delete(0,END)


# Create query function
def query():
    
    # create and connect to the database
    conn = sqlite3.connect('database.db')

    c = conn.cursor()
    
    # Query the database
    c.execute("SELECT * , oid FROM users")
    records = c.fetchall()
    
    # Loop thru results
    print_records=''
    
    for record in records:
        print_records += str(record[0]+ " " + str(record[1])+ " " + "\t" + str(record[3]) + "\n" )

    query_label = Label(root,text=print_records)
    query_label.grid(row=15,column=0, columnspan=2, pady=10,padx=10,ipadx=137)
    
    conn.commit()

    conn.close()

# Create text boxes
f_name = Entry(root,width=30)
f_name.grid(row=0,column=1, padx=20,pady=(10,0))

l_name = Entry(root, width=30)
l_name.grid(row=2, column=1)

w_address = Entry(root, width=30)
w_address.grid(row=4, column=1)

delete_box = Entry(root, width =30)
delete_box.grid(row=12, column=1)

# create text box labels
f_name_label = Label(root, text="First Name")
f_name_label.grid(row=0,column=0,pady=(10,0))

l_name_label = Label(root, text="Last Name")
l_name_label.grid(row=2,column=0)

w_address_label = Label(root, text="Wallet address")
w_address_label.grid(row=4,column=0)

delete_box_label = Label(root, text="ID Number")
delete_box_label.grid(row=12, column=0, pady=5)
# Create submit button
submit_btn = Button(root, text="Submit and check whats in the wallet", command=submit)
submit_btn.grid(row=6, column=0, columnspan=2,pady=10,padx=10,ipadx=100)

# Create a query button
query_btn = Button(root, text="Show Records", command=query)
query_btn.grid(row=8,column=0, columnspan=2,pady=10,padx=10,ipadx=137)

# Create delete button
delete_btn = Button(root, text="Delete user by ID", command=delete)
delete_btn.grid(row=13,column=0, columnspan=2,pady=5,padx=10,ipadx=137)

# Create update button
update_btn = Button(root, text="Edit Record", command=edit)
update_btn.grid(row=16,column=0, columnspan=2,pady=10,padx=10,ipadx=143)


launchpad_button =Button(root, text="Launchpad Collections", command=launchpad)
launchpad_button.grid (row=2 ,column=5,columnspan=2,pady=10,padx=10)

check_wallet_balance = Button(root, text="Check your wallet balance", command= escrow)
check_wallet_balance.grid (row=4, column = 5, columnspan = 2, pady = 10, padx = 10)

# commit changes
conn.commit()

# close connection
conn.close()

def popup():
    messagebox.showinfo("About me", "My name is Sem and i made this GUI as a part of a school project. We had the opportunity to choose our own subject to and this is what i made, i hope you like it! ")


about_me =Button(root, text="About me", command=popup)
about_me.grid(row=6, column= 5, columnspan=2, pady=10, padx=10)

root.mainloop()
